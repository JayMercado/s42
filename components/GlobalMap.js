import React, { useState, useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;

export default function GlobalMap({country}){

    const mapContainerRef = useRef(null);

    // States for the mapbox properties
    const [latitude, setLatitude] = useState(0);
    const [longitude, setLongitude] = useState(0);
    const [zoom, setZoom] = useState(0);

    fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country.country_name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
    .then(res => res.json())
    .then(data => {
        console.log(data)
        setLongitude(data.features[0].center[0]);
        setLatitude(data.features[0].center[1]);
        setZoom(1);
    })

    useEffect(() => {
        // Instantiating a new Mapbox Map object
        const map = new mapboxgl.Map({
            // Set the container for the map
            container: mapContainerRef.current,
            // Style options for the map
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [longitude, latitude],
            zoom: zoom
        })
        // Add navigation control (the +/ - zoom buttons)
        map.addControl(new mapboxgl.NavigationControl(), `bottom-right`);

        // Create a marker centered on the designated longitude and latitude
        const marker = new mapboxgl.Marker()
            .setLngLat([longitude, latitude])
            .addTo(map)

        // Clean up and release all resources associated with this map when component unmounts; not doing so will result in an ever-increasing consumption of memory/RAM, eventually crashing your device.
        return () => map.remove()

    }, [longitude, latitude])

    return (
        <div className="mapContainer" ref={mapContainerRef} />
    )
}