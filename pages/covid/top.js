import React, { useRef, useEffect } from 'react';
import mapboxgl from 'mapbox-gl';
import { Row, Col } from 'react-bootstrap';
import toNum from '../../helpers/toNum';
import { Doughnut } from 'react-chartjs-2';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;

export default function Top({ data }) {

    const countriesStats = data.countries_stat;
    // sorting in descending order by num of cases
    const sortedCountriesStats = countriesStats.sort((a, b) => toNum(b.cases) - toNum(a.cases));

    // Getting just the top 10
    const top = sortedCountriesStats.slice(0, 10);
    const top_names = top.map(country => country.country_name);
    const top_cases = top.map(country => toNum(country.cases));

    const mapContainerRef = useRef(null);

    useEffect(() => {
        // Instantiating a new Mapbox Map object
        const map = new mapboxgl.Map({
            // Set the container for the map
            container: mapContainerRef.current,
            // Style options for the map
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [44.63, 28.77],
            zoom: 1
        })
        // Add navigation control (the +/ - zoom buttons)
        map.addControl(new mapboxgl.NavigationControl(), `bottom-right`);

        // Create a marker centered on the designated longitude and latitude 
        top_names.forEach(country => {
            fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country}.json?access_token=${mapboxgl.accessToken}`)
            .then(res => res.json())
            .then(data => {
                const marker = new mapboxgl.Marker()
                .setLngLat([data.features[0].center[0], data.features[0].center[1]])
                .addTo(map)
            })
          })
     


        // Clean up and release all resources associated with this map when component unmounts; not doing so will result in an ever-increasing consumption of memory/RAM, eventually crashing your device.
        return () => map.remove()

    }, [])

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} md={6}>
                    <Doughnut data={{
                        datasets: [{
                            data: top_cases,
                            backgroundColor: ["red", "orange", "yellow", "green", "blue", "indigo", "violet", "purple", "brown", "pink"]
                        }],
                        labels: top_names
                    }} redraw={false} />
                </Col>
                <Col xs={12} md={6}>
                    <div className="mapContainer" ref={mapContainerRef} />
                </Col>
            </Row>
        </React.Fragment >
    )

}

export async function getStaticProps() {
    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
            "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
        }
    })

    const data = await res.json()

    return {
        props: {
            data
        }
    }
}