// Funtion for converting string data from the endpoint to numbers
export default function toNum(str) {
    // Convert the string to an array to access array methods
    const stringArray =[...str]

    // Filter out the commas in the string
    const filteredArray = stringArray.filter(element => element !== ",");

    // Reduce the filtered array to a single string without commas
    // parseInt() converts the string into a number
    return parseInt(filteredArray.reduce((x, y)=> x + y))
}